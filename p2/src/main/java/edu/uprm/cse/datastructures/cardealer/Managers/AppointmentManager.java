package edu.uprm.cse.datastructures.cardealer.Managers;

import edu.uprm.cse.datastructures.cardealer.Managers.ListManagers.AppointmentListManager;
import edu.uprm.cse.datastructures.cardealer.model.Appointment;
import edu.uprm.cse.datastructures.cardealer.util.positionallist.DLLPositionalList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("appointment")
public class AppointmentManager {

    AppointmentListManager manager = new AppointmentListManager();

    @POST
    @Path("/add/{day}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addAppointment(Appointment a, @PathParam("day") int day) {

        return manager.add(a,day);
    }

    @PUT
    @Path("{id}/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePerson(Appointment a, @PathParam("id") long id) {
        return manager.updateItem(id , a);
    }

    @DELETE
    @Path("/{id}/delete/{day}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePerson(@PathParam("id") long id , @PathParam("day") String day) {
        return manager.deleteItem(id , day);
    }

    @GET
    @Path("/day/{day}")
    @Produces(MediaType.APPLICATION_JSON)
    public Appointment[] getDay(@PathParam("day")String day){

        DLLPositionalList<Appointment> temp = manager.getDay(day);
        return temp.toArray(new Appointment[temp.size()]);
    }


    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Appointment[] getAllAppointment() {
        return manager.getAllItems();
    }

    @GET
    @Path("/{id}/{day}")
    @Produces(MediaType.APPLICATION_JSON)
    public Appointment getAppointment(@PathParam("id") long id , @PathParam("day") String day) {
        return manager.getItem(id , day).getElement();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Appointment getAppointmentAnyday(@PathParam("id") long id ) {
        return manager.getItem(id).getElement();
    }
}
