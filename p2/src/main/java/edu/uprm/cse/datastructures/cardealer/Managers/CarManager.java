package edu.uprm.cse.datastructures.cardealer.Managers;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.Managers.ListManagers.CarListManager;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.Comparators.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;


import javax.ws.rs.*;
import java.util.ArrayList;

@Path("/cars")
public class CarManager {

	static CircularSortedDoublyLinkedList<Car> cList = new CircularSortedDoublyLinkedList<>(new CarComparator());
    CarListManager manager = new CarListManager(cList);
	// private final CopyOnWriteArrayList<Car> cList =
	// MockCarList.getInstance();

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {

		return manager.addItem(car);
	}

	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car , @PathParam("id") long id) {

		return manager.updateItem(manager.getItem(id) , car);
	}

	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) {
		return manager.deleteItem(manager.getItem(id));
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Car> getAllCars() {

		return manager.getAllItems();

	}

	@GET
	@Path("/brand/{Brand}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getCarsBrand(@PathParam("Brand") String Brand){
		return manager.getBrand(Brand);
	}

	@GET
	@Path("/year/{year}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getCarsYear(@PathParam("year") int year){
		return manager.getYear(year);
	}


	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		return manager.getItem(id);
	}

}
