package edu.uprm.cse.datastructures.cardealer.Managers;

import edu.uprm.cse.datastructures.cardealer.Managers.ListManagers.CarUnitListManager;
import edu.uprm.cse.datastructures.cardealer.Managers.ListManagers.ItemManager;
import edu.uprm.cse.datastructures.cardealer.model.CarUnit;
import edu.uprm.cse.datastructures.cardealer.model.Comparators.CarUnitComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/carunit")
public class CarUnitManager {
    static CircularSortedDoublyLinkedList<CarUnit> cList = new CircularSortedDoublyLinkedList<>(new CarUnitComparator());
    ItemManager<CarUnit> manager = new CarUnitListManager(cList);


    @POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCarUnit(CarUnit car) {

        return manager.addItem(car);
    }

    @PUT
    @Path("{id}/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCarUnit(CarUnit car , @PathParam("id") long id) {
        System.out.println("Entered update method ");
        return manager.updateItem(manager.getItem(id) , car);
    }

    @DELETE
    @Path("/{id}/delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCarUnit(@PathParam("id") long id) {
        return manager.deleteItem(manager.getItem(id));
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<CarUnit> getAllCars() {

        return manager.getAllItems();

    }



    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public CarUnit getCarUnit(@PathParam("id") long id) {
        return manager.getItem(id);
    }

    public static CircularSortedDoublyLinkedList<CarUnit> getList() {
        return cList;
    }
}


