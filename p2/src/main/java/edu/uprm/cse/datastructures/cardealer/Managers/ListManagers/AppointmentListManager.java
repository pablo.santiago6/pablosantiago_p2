package edu.uprm.cse.datastructures.cardealer.Managers.ListManagers;

import edu.uprm.cse.datastructures.cardealer.JsonError;
import edu.uprm.cse.datastructures.cardealer.model.Appointment;
import edu.uprm.cse.datastructures.cardealer.model.CarUnit;
import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.positionallist.DLLPositionalList;
import edu.uprm.cse.datastructures.cardealer.util.positionallist.Position;
import edu.uprm.cse.datastructures.cardealer.util.positionallist.*;

import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;


/**
 * this is the only manager that does not extends ItemManager because of how different they work
 *
 * this one works with 5 different positinal list that repesent the days of a week
 */
public class AppointmentListManager {

    static DLLPositionalList<Appointment>[] schedule  = new DLLPositionalList[5];

    int monday = 0, tuesday = 1, wednesday = 2 , thursday = 3 , friday = 4;

    static DLLPositionalList<Appointment> mL = new DLLPositionalList<>();
    static DLLPositionalList<Appointment> tL = new DLLPositionalList<>();
    static DLLPositionalList<Appointment> wL = new DLLPositionalList<>();
    static DLLPositionalList<Appointment> thL = new DLLPositionalList<>();
    static DLLPositionalList<Appointment> fL = new DLLPositionalList<>();
//
//    static {
//        for (int i = 0; i < 5; i++) {
//            schedule[i] = new DLLPositionalList<>();
//        }
//    }

    public AppointmentListManager(){

//        for (int i = 0; i < 5; i++) {
//            schedule[i] = new DLLPositionalList<>();
////            schedule[0].addLast(new Appointment());
//        }

        schedule[monday] = mL;
        schedule[tuesday] = tL;
        schedule[wednesday] = wL;
        schedule[thursday] = thL;
        schedule[friday] = fL;
    }
    public Response add(Appointment a , int day){
        if(!validate(a, day))throw new NotAcceptableException("Unit already exists");

//        Position<Appointment> newPos = new Node<Appointment>(a);
        schedule[day].addLast(a);
        return Response.status(201).build();
    }


    public DLLPositionalList<Appointment> getDay(String day){

        switch (day.toLowerCase()){

            case "monday":
                return schedule[monday];
            case "tuesday":
                return schedule[tuesday];
            case "wednesday":
                return schedule[wednesday];
            case "thursday":
                return schedule[thursday];
            case "friday":
                return schedule[friday];
        }

        throw new IllegalArgumentException("Not a valid day");
    }
    public Appointment[] getAllItems() {
        ArrayList<Appointment> all = new ArrayList<>();
        DLLPositionalList<Appointment> temp;
        for (int i = 0; i < 5; i++) {

            temp = schedule[i];

            for (Position<Appointment> p : temp) {
                all.add(p.getElement());
            }
        }

        return all.toArray(new Appointment[0]);
    }
    public Position<Appointment> getItem(long id) {

        DLLPositionalList<Appointment> temp;
        for (int i = 0; i < 5; i++) {
            temp = schedule[i];

            for (Position p : temp) {
                if(((Appointment)p.getElement()).getAppointmentId() == id)return p;
            }

        }
        throw new NotFoundException(new JsonError("Error", "Appointment" + id + " not found"));

    }

    public Position<Appointment> getItem(long id , String day) {

        DLLPositionalList<Appointment> temp;
            temp = getDay(day);

            for (Position p : temp) {
                if(((Appointment)p.getElement()).getAppointmentId() == id)return p;
            }

        throw new NotFoundException(new JsonError("Error", "Appointment" + id + " not found"));

    }
    public Response updateItem(long id, Appointment newer){

        if(id != newer.getAppointmentId())return Response.status(Response.Status.BAD_REQUEST).build();

        Position<Appointment> position = getItem(id);
        ((Node<Appointment>)position).setElement(newer);
        return Response.status(Response.Status.OK).build();

    }
    public Response deleteItem(long id ,String day ){
        Position<Appointment> position = getItem(id);
//        int day =getDayof(position.getElement().getAppointmentId());
        getDay(day).remove(position);

        return Response.status(Response.Status.OK).build();
    }
    private int getDayof(long id){
        DLLPositionalList<Appointment> temp;
        for (int i = 0; i < 5; i++) {
            temp = schedule[i];

            for (Position<Appointment> p : temp) {
                if(p.getElement().getAppointmentId() == id)return i;
            }

        }

        throw new IllegalArgumentException("Not here");
    }



    public boolean validate(Appointment appointment , int day) {
        for (Position<Appointment> p :
                schedule[day]) {
            if(p.getElement().getAppointmentId() == appointment.getAppointmentId())return false;
        }


        /**
         * this code is a  implementation that verifies that the carUnit and Person associated to the appointment exist
         * may not be needed , specs are not specific on this
         */
        CircularSortedDoublyLinkedList<CarUnit> list = edu.uprm.cse.datastructures.cardealer.Managers.CarUnitManager.getList();

        boolean carUnitExists = false;

//        for (CarUnit cu :
//                list) {
//            if(cu.getCarUnitId() == appointment.getCarUnitId()){
//                carUnitExists = true;
//                break;
//            }

        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getCarUnitId() == appointment.getCarUnitId()) {
                carUnitExists = true;
                break;
            }
        }

        if(!carUnitExists){
            throw new NotFoundException(new JsonError("Bad Appointment","The car unit  "+ appointment.getCarUnitId()+" associated to the appointment does not exist"));
        }


        return true;
    }
}
