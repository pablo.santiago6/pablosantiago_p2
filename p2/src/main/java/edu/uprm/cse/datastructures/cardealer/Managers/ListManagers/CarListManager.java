package edu.uprm.cse.datastructures.cardealer.Managers.ListManagers;

import edu.uprm.cse.datastructures.cardealer.JsonError;
import edu.uprm.cse.datastructures.cardealer.model.*;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;


/**
 * this manager extends item manager and adds a few unique methods for dealing with cars
 */
public class CarListManager extends ItemManager<Car> {

    public CarListManager(CircularSortedDoublyLinkedList<Car> list){
        super(list);
    }

    public Car[] getBrand(String brand){
        ArrayList<Car> cars = new ArrayList<>();
        for (Car c : super.list) {
            if(c.getCarBrand().toLowerCase().equals(brand.toLowerCase()))cars.add(c);
        }

        return cars.toArray(new Car[0]);
    }

    public Car[] getYear(int year){
        ArrayList<Car> cars = new ArrayList<>();
        for (Car c : super.list) {
            if(c.getcarYear() == year)cars.add(c);
        }

        return cars.toArray(new Car[0]);
    }

//    @Override
//    public Car[] getAllItems() {
//        return super.list.toArray(new Car[list.size()]);
//    }

    @Override
    public boolean validate(Car car) {
        if(car.getCarId() < 0  || car.getCarBrand().isEmpty()
                || car.getCarBrand() == null || car.getCarPrice() < 0 || car.getCarModel().isEmpty()
                || car.getCarModel() == null )return false;

        return true;
    }

//    @Override
//    public Car getItem(long id) {
//        for (Car car : super.list) {
//            if (car.getCarId() == id) {
//                return car;
//            }
//        }
//        throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
//    }
}
