package edu.uprm.cse.datastructures.cardealer.Managers.ListManagers;

import edu.uprm.cse.datastructures.cardealer.JsonError;
import edu.uprm.cse.datastructures.cardealer.model.CarUnit;
import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

import javax.ws.rs.NotFoundException;

public class CarUnitListManager extends ItemManager<CarUnit> {

    public CarUnitListManager(CircularSortedDoublyLinkedList list){
        super(list);
    }


//    @Override
//    public CarUnit[] getAllItems() {
//        return super.list.toArray(new CarUnit[super.list.size()]);
//    }

//    @Override
//    public CarUnit getItem(long ID) {
//        for (CarUnit cu : super.list) {
//            if(cu.getCarId() == ID)return cu;
//        }
//        throw new NotFoundException(new JsonError("Error", "CarUnit:  " + ID + " not found"));
//    }

    @Override
    public boolean validate(CarUnit carUnit) {
        if(carUnit.getVIN().isEmpty()
                || carUnit.getVIN() == null
                || carUnit.getColor() == null){
            return false;
        }

        boolean personIsIn =false;

        CircularSortedDoublyLinkedList<Person> list = edu.uprm.cse.datastructures.cardealer.Managers.PersonManager.getList();

        /**
         * this code is a  implementation that verifies that the carUnit and Person associated to the appointment exist
         * may not be needed , specs are not specific on this
         */
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getPersonId() == carUnit.getPersonId()) {
                personIsIn = true;
                break;
            }
        }
        if(!personIsIn){
            throw new NotFoundException(new JsonError("Bad Appointment","The person "+ carUnit.getPersonId())+" associated to the car unit does not exist");
        }


        return true;
    }
}
