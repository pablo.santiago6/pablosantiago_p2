package edu.uprm.cse.datastructures.cardealer.Managers.ListManagers;

import edu.uprm.cse.datastructures.cardealer.model.Ideable;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import edu.uprm.cse.datastructures.cardealer.JsonError;


/**
 * This is class is base for other list managers to expand upon , it uses a CircularSorterDoubleLinkedList
 *      as a storage for the elements
 * @param <E>
 */
public abstract class ItemManager<E> {


    protected CircularSortedDoublyLinkedList<E> list ;

    public ItemManager(CircularSortedDoublyLinkedList<E> list){

        this.list = list;
    }




    public Response addItem(E e) throws ClassCastException{


        try {

//            for (E item : this.list) {
//                if (((Ideable) item).getId() == ((Ideable) e).getId()) {
//                    return Response.status(Response.Status.CONFLICT).build();
//                }
//            }

            for (int i = 0; i < this.list.size(); i++) {
                if (((Ideable) list.get(i)).getId()
                        == ((Ideable) e).getId()) {
                    return Response.status(Response.Status.FORBIDDEN).build();
                }
            }

        }catch (Exception error){
            System.out.println(error);
        }

        if(!validate(e)){

            Response.status(Response.Status.CONFLICT).build();
        }
        list.add(e);
        return Response.status(201).build();
    }




    public Response updateItem(E oldElement,E newElement ) {


//        System.out.println(list.remove(oldElement));
        if(!list.remove(oldElement))return Response.status(Response.Status.NOT_FOUND).build();
        list.add(newElement);
        return Response.status(Response.Status.OK).build();

    }


    public Response deleteItem(E e) {
        if(list.remove(e))return Response.status(Response.Status.OK).build();
        return Response.status(Response.Status.NOT_FOUND).build();
    }



    public ArrayList<E> getAllItems(){

        ArrayList<E> items = new ArrayList<>();

        for (E e : list) {
            items.add(e);
        }

        return items;
    }

    public E getItem(long ID){
        for (E e :
                this.list) {
            if(((Ideable)e).getId() == ID)return e;
        }

        throw new NotFoundException(new JsonError("Error", "Element:" + ID + " not found"));
    }

    public abstract boolean validate(E e);
}
