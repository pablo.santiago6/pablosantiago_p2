package edu.uprm.cse.datastructures.cardealer.Managers.ListManagers;

import edu.uprm.cse.datastructures.cardealer.JsonError;
import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;

public class PersonListManager extends ItemManager<Person> {

    public PersonListManager(CircularSortedDoublyLinkedList list){
        super(list);
    }


    public ArrayList<Person> getLastName(String ln){
        ArrayList<Person> list = new ArrayList<>();
        for (Person p :super.list ) {
            if(p.getLastName().toLowerCase().equals(ln))list.add(p);
        }
        return list;
    }

    public ArrayList<Person> getFirstName(String fn){
        ArrayList<Person> list = new ArrayList<>();
        for (Person p :super.list ) {
            if(p.getFirstName().toLowerCase().equals(fn))list.add(p);
        }
        return list;
    }


//    @Override
//    public Person[] getAllItems() {
//        return super.list.toArray(new Person[list.size()]);
//    }

//    @Override
//    public Person getItem(long ID) {
//        for (Person person : super.list) {
//            if(person.getPersonId() == ID)return  person;
//        }
//
//        throw new NotFoundException(new JsonError("Error", "Person " + ID + " not found"));
//    }

    @Override
    public boolean validate(Person person) {


        if(person.getFirstName() == null
                || person.getLastName() == null
                || person.getPhone() == null
                ){
            return false;
        }

        return true;
    }
}
