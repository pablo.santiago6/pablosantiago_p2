package edu.uprm.cse.datastructures.cardealer.Managers;


import edu.uprm.cse.datastructures.cardealer.Managers.ListManagers.PersonListManager;
import edu.uprm.cse.datastructures.cardealer.model.Comparators.PersonComparator;
import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/person")
public class PersonManager {
    static CircularSortedDoublyLinkedList<Person> list = new CircularSortedDoublyLinkedList<>(new PersonComparator());

    PersonListManager manager= new PersonListManager(list);

    @POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addPerson(Person person) {

        return manager.addItem(person);

    }

    @PUT
    @Path("{id}/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePerson(Person person , @PathParam("id") long id) {
        return manager.updateItem(manager.getItem(id) , person);
    }

    @DELETE
    @Path("/{id}/delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePerson(@PathParam("id") long id) {
        return manager.deleteItem(manager.getItem(id));
    }

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Person> getAllPersons() {
        return manager.getAllItems();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Person getPerson(@PathParam("id") long id) {
        return manager.getItem(id);
    }

    @GET
    @Path("/lastname/{lastName}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Person> getLastName(@PathParam("lastName") String lastName){
        return manager.getLastName(lastName.toLowerCase());

    }
    @GET
    @Path("/firstname/{firstName}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Person> getFirstName(@PathParam("firstName") String firstName){
        return manager.getFirstName(firstName.toLowerCase());

    }

    public static CircularSortedDoublyLinkedList<Person> getList() {
        return list;
    }
}
