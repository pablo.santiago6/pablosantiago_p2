package edu.uprm.cse.datastructures.cardealer.model;


import java.util.Objects;

public class Appointment implements Ideable<Appointment>{
    private long appointmentId; // internal id of the appointment
    private long carUnitId; // id of the car to be serviced
    private String job; // description of the job to be done (i.e.: “oil change”)
    private double bill = 0; // cost of the service (initially 0).
    private String day;

    public Appointment(long appointmentId, String day ,long carUnitId, String job, double bill) {
        super();
        this.appointmentId = appointmentId;
        this.carUnitId = carUnitId;
        this.job = job;
        this.bill = bill;
        this.day = day;
    }

    public Appointment(){ }

    public long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public long getCarUnitId() {
        return carUnitId;
    }

    public void setCarUnitId(long carUnitId) {
        this.carUnitId = carUnitId;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public double getBill() {
        return bill;
    }

    public void setBill(double bill) {
        this.bill = bill;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Appointment)) return false;
        Appointment that = (Appointment) o;
        return getAppointmentId() == that.getAppointmentId() &&
                getCarUnitId() == that.getCarUnitId() &&
                Double.compare(that.getBill(), getBill()) == 0 &&
                Objects.equals(getJob(), that.getJob()) &&
                Objects.equals(day, that.day);
    }

    @Override
    public int hashCode() {

        return Objects.hash(getAppointmentId(), getCarUnitId(), getJob(), getBill(), day);
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "appointmentId=" + appointmentId +
                ", carUnitId=" + carUnitId +
                ", job='" + job + '\'' +
                ", bill=" + bill +
                ", day='" + day + '\'' +
                '}';
    }

    @Override
    public long getId() {
        return this.getAppointmentId();
    }
}
