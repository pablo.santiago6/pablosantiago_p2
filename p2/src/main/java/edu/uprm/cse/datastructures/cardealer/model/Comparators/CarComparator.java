package edu.uprm.cse.datastructures.cardealer.model.Comparators;

import java.util.Comparator;
import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CarComparator implements Comparator<Car> {

	public int compare(Car o1 , Car o2) {
		
		
		String car1 = o1.getCarBrand() + o1.getCarModel() + o1.getCarModelOption();
		String car2 = o2.getCarBrand() + o2.getCarModel() + o2.getCarModelOption();
		
		return car1.compareTo(car2);
	}
}
