package edu.uprm.cse.datastructures.cardealer.model.Comparators;

import edu.uprm.cse.datastructures.cardealer.model.CarUnit;

import java.util.Comparator;

public class CarUnitComparator implements Comparator<CarUnit>{
    @Override
    public int compare(CarUnit carUnit, CarUnit t1) {
        return carUnit.getVIN().compareTo(t1.getVIN());
    }
}
