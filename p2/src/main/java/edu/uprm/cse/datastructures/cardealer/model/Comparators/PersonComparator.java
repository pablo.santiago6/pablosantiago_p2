package edu.uprm.cse.datastructures.cardealer.model.Comparators;

import edu.uprm.cse.datastructures.cardealer.model.Person;

import java.util.Comparator;

public class PersonComparator implements Comparator<Person>{

    @Override
    public int compare(Person person, Person t1) {
        int r = person.getLastName().compareTo(t1.getLastName());
        if(r != 0)return r;

        else return person.getFirstName().compareTo(t1.getFirstName());
    }
}
