package edu.uprm.cse.datastructures.cardealer.model;

public interface Ideable<E>  {

    public long getId();

}
