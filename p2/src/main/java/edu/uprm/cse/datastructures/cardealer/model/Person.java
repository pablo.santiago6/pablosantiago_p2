package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Objects;

public class Person implements Ideable<Person>{
    private long personId; // internal id of the person
    private String firstName; // first name
    private String lastName; // lastname
    private Integer age; // age
    private char gender; // gender
    private String phone; // phone number

    //Default Constructor
    public Person(){}




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return getPersonId() == person.getPersonId() &&
                getGender() == person.getGender() &&
                Objects.equals(getFirstName(), person.getFirstName()) &&
                Objects.equals(getLastName(), person.getLastName()) &&
                Objects.equals(getAge(), person.getAge()) &&
                Objects.equals(getPhone(), person.getPhone());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getPersonId() , getFirstName() , getLastName() , getAge() , getGender() , getPhone());
    }

    public Person(long personId, String firstName, String lastName, Integer age, char gender, String phone) {
        this.personId = personId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;

        this.phone = phone;
    }

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Person{" +
                "personId=" + personId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", phone='" + phone + '\'' +
                '}';
    }

    @Override
    public long getId() {
        return this.getPersonId();
    }
}
