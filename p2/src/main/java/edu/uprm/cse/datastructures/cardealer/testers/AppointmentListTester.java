package edu.uprm.cse.datastructures.cardealer.testers;

import edu.uprm.cse.datastructures.cardealer.Managers.ListManagers.AppointmentListManager;
import edu.uprm.cse.datastructures.cardealer.model.Appointment;
import edu.uprm.cse.datastructures.cardealer.util.positionallist.Position;

public class AppointmentListTester {

    public static void main(String[] args) {

        AppointmentListManager manager = new AppointmentListManager();

//        manager.add(new Appointment() , "Monday");
//
//        manager.add(new Appointment() ,"Monday");

        for (Position<Appointment> p : manager.getDay("monday")) {
            System.out.println(p.getElement());
        }


    }
}
