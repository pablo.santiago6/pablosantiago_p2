package edu.uprm.cse.datastructures.cardealer.testers;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class IteratorTester {

    public static void main(String[] args){
        CircularSortedDoublyLinkedList<Integer> list = new CircularSortedDoublyLinkedList<>(Integer::compareTo);

        for (int i = 0; i <=100; i++) {
            list.add(i);
        }

        for (Integer i: list) {
            System.out.println(i);
        }

    }
}
