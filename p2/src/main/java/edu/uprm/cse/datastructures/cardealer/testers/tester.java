package edu.uprm.cse.datastructures.cardealer.testers;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.Comparators.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.*;

public class tester {

	public static void main(String[] args) {
		
		CircularSortedDoublyLinkedList<Car> list = new CircularSortedDoublyLinkedList<>(new CarComparator());
		
//		long carId, String carBrand, String carModel, String carModelOption, double carPrice
		
		
		
		Car car0 = new Car(3,"BMW","i8","m2",10 , 10);
		Car car1 = new Car(1,"BMW","i8","m3",10,2090);
		Car car2 = new Car(2,"BMW","a8","m2",10,89);
		Car car3 = new Car(0,"Ferraro","cherry","cool",23,7);
		Car car4 = new Car(4,"Audi","SomeModel","SomeOption",100,2);
		
		System.out.println("Adding 3 cars to list");
		list.add(car0);
		list.add(car3);
		list.add(car2);
		list.add(car1);
		list.add(car4);
		list.printList();
		
		
		System.out.println("----------------------------------------------------");
		System.out.println(list.firstIndex(car1)+ " First index of "+car1.toString());
		System.out.println(list.lastIndex(car1)+ " last index of "+car1.toString());
		System.out.println("----------------------------------------------------");
		
		System.out.println("----------------------------------------------------");
		System.out.println("Testin get at index 3 ");
		System.out.println(list.get(3).toString());
		System.out.println("----------------------------------------------------");
		
		System.out.println("----------------------------------------------------");
		System.out.println("testing iterator");
		
		for(Car c:list){
			System.out.println(c.toString());
		}
		System.out.println("----------------------------------------------------");
		System.out.println("Testing toArray");
		Car[] arr = list.toArray(new Car[list.size()]);
		
		for(int i = 0; i < arr.length;i++){
			System.out.println(arr[i].toString());
		}
		System.out.println("----------------------------------------------------");
		
		
		
		list.clear();
		
		list.printList();
		
		
		
	}
}
