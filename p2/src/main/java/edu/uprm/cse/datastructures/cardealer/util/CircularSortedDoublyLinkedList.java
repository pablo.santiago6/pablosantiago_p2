package edu.uprm.cse.datastructures.cardealer.util;

import java.util.*;

//import edu.uprm.cse.datastructures.cardealer.util.DoublyLinkedList.Node;
import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> , Iterable<E> {

	// Inner class for nodes
	public static class Node<E> {
		private Node<E> prev;

		public Node<E> getPrev() {
			return prev;
		}

		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		private Node<E> next;
		private E element;

		public Node(Node<E> prev, E element, Node<E> next) {
			this.prev = prev;
			this.next = next;
			this.element = element;
		}

		public void clear() {
			this.element = null;
			this.setPrev(null);
			this.setNext(null);
		}

		public Node() {
			this(null, null, null);
		}

	}

	private Node<E> header;
	private int size;
	private Comparator<E> cmp;

	public CircularSortedDoublyLinkedList(Comparator<E> cmp) {

		this.cmp = cmp;
		header = new Node<E>(header , null , header);

	}

	@Override
	public Iterator<E> iterator() {
		Iterator<E> itr = new Iterator<E>(){

			private Node<E> current = header;
			
			@Override
			public boolean hasNext() {
				return current.getNext()!=header;
			}

			@Override
			public E next() {
				// TODO Auto-generated method stub
				current = current.getNext();
				
				return current.getElement();
				
			}
		
			
		};
		return itr;
	}

	@Override
	public boolean add(E obj) {
//		if ((obj instanceof Comparable)) {

			if (this.size == 0) {

				Node<E> newNode = new Node<E>(header, obj, header);
				header.setNext(newNode);
				header.setPrev(newNode);
				size++;

				return true;
			}

//			Comparator<Car> cmp = new CarComparator();

			Node<E> cursor = this.header.getNext();

			while (cursor != header) {


//				
				if (cmp.compare(obj,cursor.getElement()) > 0 && cursor.getNext() != header) {
					
					cursor = cursor.getNext();

				}

				else {

					if (cmp.compare(obj,cursor.getElement()) > 0 ) {
						cursor = cursor.getNext(); // cursor will become header because this element should be last
					}

					Node<E> after = cursor;

					Node<E> before = cursor.getPrev();

					Node<E> newNode = new Node<>(before, obj, after);

					before.setNext(newNode);

					after.setPrev(newNode);

					size++;

					return true;
				}

			}

//		}
		return false;

	}
	


	
	public int indexOf(E e){
		return firstIndex(e);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return this.size;
	}

	@Override
	public boolean remove(E obj) {

			Node<E> cursor;

			for (cursor = this.header.getNext(); cursor != header; cursor = cursor.getNext()) {

				if (obj.equals(cursor.getElement())) {

					cursor.getPrev().setNext(cursor.getNext());
					cursor.getNext().setPrev(cursor.getPrev());
					size--;
					return true;

				}

			}

		return false;
	}

	@Override
	public boolean remove(int index) {

		if (this.size == 0)
			return false;

		Node<E> ntr = getNodeAt(index);

		ntr.setPrev(ntr.getNext());
		ntr.setNext(ntr.getNext());

		size--;
		return true;

	}

	@Override
	public int removeAll(E obj) {
		// TODO Auto-generated method stub
		int count = 0;
		for(E e: this) {
			if(e.equals(obj)) {
				this.remove(obj);
				count++;
			}
		}
		
		return count;
	}

	@Override
	public E first() {
		// TODO Auto-generated method stub
		if (this.size == 0)
			return null;

		return header.getNext().getElement();
	}

	@Override
	public E last() {
		// TODO Auto-generated method stub
		if (this.size == 0)
			return null;

		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) {

		if (isEmpty())
			return null;

		return getNodeAt(index).getElement();
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub

		while (!isEmpty()) {
			this.remove(0);
		}

	}

	@Override
	public boolean contains(E e) {

		Node<E> cursor = this.header.getNext();

		while (cursor.getNext() != header) {
			if (cursor.getElement().equals(e))
				return true;
			cursor = cursor.getNext();
		}

		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return this.size == 0;
	}

	@Override
	public int firstIndex(E e) {

//		Node<E> cursor = this.header.getNext();
		int i = 0;

		for (Node<E> cursor = this.header.getNext(); cursor != this.header; cursor = cursor.getNext())

//		while(cursor.getNext() != this.header)
		{
			if (cursor.getElement().equals(e)) {
				return i;
			}
			i++;
		}

		return -1;
	}

	@Override
	public int lastIndex(E e) {
		// TODO Auto-generated method stub
		int index = this.firstIndex(e);
		Node<E> cursor = this.getNodeAt(index);

		while (cursor.getNext().equals(e)) {
			index++;
			cursor = cursor.getNext();
		}
		return index;
	}

	public void printList() {
		Node<E> cursor = this.header.getNext();

		if (this.isEmpty()) {
			System.out.println("List is Empty");
			return;
		}
		int index = 1;
		System.out.println("\nList size: " + this.size());
		while (cursor != header) {
			System.out.println("----------------------------------------------------------------------");
			System.out.println(index + ": " + cursor.getElement().toString());
			System.out.println("----------------------------------------------------------------------");
			cursor = cursor.getNext();
			index++;
		}
		System.out.println("\n");
	}
	
	public <T1> T1[]  toArray(T1[] arr) {
		
//		arr = (T1[]) new Object[this.size];
		
		Node<E> cursor = this.header.getNext();
		
		for(int i = 0 ; i < arr.length ; i++) {
			arr[i] =  (T1) cursor.getElement();
			cursor = cursor.getNext();
		}
		return arr;
	}

	private Node<E> getNodeAt(int index) {

		if (index < 0 || index >= this.size) {
			return null;
		}

		Node<E> cursor = this.header.getNext();

		for (int i = 0; i < index; i++) {
			cursor = cursor.getNext();
		}
		return cursor;
	}

}
