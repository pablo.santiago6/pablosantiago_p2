package edu.uprm.cse.datastructures.cardealer.util;

public class DoublyLinkedList<T> implements IndexList<T> {


    //Inner class for nodes
    public static class Node<T>{
        private Node<T> prev;

        public Node<T> getPrev() {
            return prev;
        }

        public void setPrev(Node<T> prev) {
            this.prev = prev;
        }

        public Node<T> getNext() {
            return next;
        }

        public void setNext(Node<T> next) {
            this.next = next;
        }

        public T getElement() {
            return element;
        }

        public void setElement(T element) {
            this.element = element;
        }

        private Node<T> next;
        private T element;

        public Node(Node prev , T element , Node next){
            this.prev = prev;
            this.next = next;
            this.element = element;
        }

        public void clear(){
            this.element= null;
            this.setPrev(null);
            this.setNext(null);
        }

        public Node(){ this(null,null ,null); }

    }

    private Node<T> header , trailer;
    private int size ;


    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() { return this.size == 0; }

    @Override
    public T get(int index) throws IndexOutOfBoundsException {
        isIndexValid(index);

        return getNodeAt(index).getElement();

    }

    @Override
    public T remove(int index) throws IndexOutOfBoundsException {
        isIndexValid(index);

        Node<T> ntr = getNodeAt(index);

        //Rearrage links
        ntr.getPrev().setNext(ntr.getNext());
        ntr.getNext().setPrev(ntr.getPrev());

        T etr = ntr.getElement();
        ntr.clear();

        this.size--;
        return etr;
    }

    @Override
    public T set(int index, T e) throws IndexOutOfBoundsException {
        isIndexValid(index);

        Node<T> node = getNodeAt(index);
        T etr = node.getElement();

        node.setElement(e);

        return etr;


    }

    @Override
    public void add(int index, T e) throws IndexOutOfBoundsException {

        /*Verify if user wants to put element at the end of list , dont send to
        isIndexValid() because it will give an IOOBE
         */
        if(index == this.size)this.add(e);


        else if(isEmpty()){
            Node<T> newNode = new Node<>();
            this.header = new Node<>(null,null,newNode);
            this.trailer = new Node<>(newNode,null,null);

            newNode.setPrev(header);
            newNode.setNext(trailer);
            newNode.setElement(e);
            this.size++;

        }

        else{
            Node<T> newNode = new Node<>();
            Node<T> nodeAfter = getNodeAt(index);
            Node<T> nodeBefore = nodeAfter.getPrev();

            newNode.setPrev(nodeBefore);
            newNode.setNext(nodeAfter);

            nodeAfter.setPrev(newNode);
            nodeBefore.setNext(newNode);
            newNode.setElement(e);
            this.size++;
        }



    }

    @Override
    public void add(T e) {
        Node<T> newNode = new Node<>();

        if(isEmpty()){
            this.header = new Node<>();
            this.trailer = new Node<>();

            newNode.setElement(e);

            this.header.setNext(newNode);
            newNode.setPrev(header);
            newNode.setNext(trailer);
            trailer.setPrev(newNode);

            this.size++;

        }
        else {
            Node<T> nodeBefore = trailer.getPrev();

            newNode.setPrev(nodeBefore);
            newNode.setElement(e);
            newNode.setNext(trailer);

            nodeBefore.setNext(newNode);
            trailer.setPrev(newNode);

            this.size++;
        }

    }

    private void isIndexValid(int index){
        if(index < 0 || index >= this.size){
            throw new IndexOutOfBoundsException("Index:" + index + " while size:"+this.size);
        }

    }
    private Node<T> getNodeAt(int index){

        //Assumes index is valid

        Node<T> cursor = this.header.getNext();

        for (int i = 0; i < index; i++) {
            cursor = cursor.getNext();
        }

        return cursor;
    }



}
