package edu.uprm.cse.datastructures.cardealer.util;

public interface IndexList<T> {

	// get number of elements in list
	public int size(); 
	
	// determine if this list is empty
	public boolean isEmpty(); 
	
	// get element at given index position
	public T get(int index) throws IndexOutOfBoundsException; 
	
	// remove element at given index position
	public T remove(int index) throws IndexOutOfBoundsException; 
	
	//set the value of element at position index
	public T set(int index, T e) throws IndexOutOfBoundsException; 
	
	//adds a new element position index, shifting follow up elements by one spot
	public void add(int index, T e) throws IndexOutOfBoundsException; 
	
	// adds a new element at the end of the list
	public void add(T e);
}
