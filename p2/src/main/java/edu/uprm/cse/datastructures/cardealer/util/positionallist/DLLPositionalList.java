package edu.uprm.cse.datastructures.cardealer.util.positionallist;

import java.lang.Iterable;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class DLLPositionalList<E> implements PositionalList<E> , Iterable<Position<E>> {


	private Node<E> header;
	private Node<E> tail;
	private int currentSize;

	public DLLPositionalList() {
		this.currentSize = 0;
		this.header = new Node<E>(null);
		this.tail = new Node<E>(null);

		this.header.setNext(this.tail);
		this.tail.setPrev(this.header);
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public Position<E> first() {
		if (this.isEmpty()) {
			return null;
		} else {
			return this.header.getNext();
		}
	}

	@Override
	public Position<E> last() {
		if (this.isEmpty()) {
			return null;
		} else {
			return this.tail.getPrev();
		}
	}

	@Override
	public Position<E> before(Position<E> p) {
		if (!this.isValid(p)) {
			return null;
		} else {
			Node<E> N = (Node<E>) p;
			return position(N.getPrev());
		}
	}

	@Override
	public Position<E> after(Position<E> p) {
		if (!this.isValid(p)) {
			return null;
		} else {
			Node<E> N = (Node<E>) p;
			return position(N.getNext());
		}
	}

	@Override
	public Position<E> addFirst(E e) {
		return this.addBetween(e, this.header, this.header.getNext());
	}

	@Override
	public Position<E> addLast(E e) {
		return this.addBetween(e, this.tail.getPrev(), this.tail);

	}

	@Override
	public Position<E> addBefore(Position<E> p, E e) {
		if (!this.isValid(p)) {
			return null;
		} else {
			Node<E> N = (Node<E>) p;
			return this.addBetween(e, N.getPrev(), N);
		}
	}

	@Override
	public Position<E> addAfter(Position<E> p, E e) {
		if (!this.isValid(p)) {
			return null;
		} else {
			Node<E> N = (Node<E>) p;
			return this.addBetween(e, N, N.getNext());
		}
	}

	@Override
	public E set(Position<E> p, E e) {
		if (!this.isValid(p)) {
			return null;
		} else {
			E result = p.getElement();
			((Node<E>) p).setElement(e); // Foooo!!!!!!;
			return result;
		}
	}

	@Override
	public E remove(Position<E> p) {
		if (!this.isValid(p)) {
			return null;
		} else {
			Node<E> temp = (Node<E>) p;
			E result = p.getElement();
			temp.getNext().setPrev(temp.getPrev());
			temp.getPrev().setNext(temp.getNext());
			temp.setNext(null);
			temp.setPrev(null);
			temp.setElement(null);
			this.currentSize--;
			return result;
		}
	}

	public <T> T[] toArray(T[] arr) {

		int i = 0;
		for (Node<E> cursor = header.getNext(); cursor.getNext() != null; cursor = cursor = cursor.getNext()) {

			arr[i++] = (T) cursor.getElement();
		}
		return arr;
	}

	private boolean isValid(Position<E> p) {
		Node<E> t = (Node<E>) p;
		return (t.getNext() != null && t.getPrev() != null);
	}

	private Position<E> position(Node<E> N) {
		if ((N == this.header) || (N == this.tail)) {
			return null; // do not return the dummy headers
		} else {
			return N;
		}
	}

	private Position<E> addBetween(E e, Node<E> prev, Node<E> next) {
		Node<E> newNode = new Node<E>(null);
		newNode.setElement(e);
		newNode.setNext(next);
		newNode.setPrev(prev);
		prev.setNext(newNode);
		next.setPrev(newNode);
		this.currentSize++;
		return newNode;
	}

	@Override
	public Iterator<Position<E>> iterator() {
		return new positionIterator();
	}


	private class positionIterator implements Iterator<Position<E>> {
		private Node<E> cursor = header.getNext(),
				recent = null;

		@Override
		public boolean hasNext() {
			return cursor != tail;
		}

		@Override
		public Position<E> next() throws NoSuchElementException {
			if (!hasNext())
				throw new NoSuchElementException("No more elements.");
			recent = cursor;
			cursor = cursor.getNext();
			return recent;
		}

	}
}
