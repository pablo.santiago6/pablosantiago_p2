package edu.uprm.cse.datastructures.cardealer.util.positionallist;

public class Node<E> implements Position<E> {

        public Node(E element, Node<E> next, Node<E> prev) {
            super();
            this.element = element;
            this.next = next;
            this.prev = prev;
        }

        public Node(E e) {
            super();
            this.element = e;
            this.next = null;
            this.prev = null;
        }

        private E element;
        private Node<E> next;
        private Node<E> prev;



        @Override
        public E getElement() {

            if (this.next == null) {
                return null;
            }
            return  this.element;
        }


        public Node<E> getNext() {
            return next;
        }


        public void setNext(Node<E> next) {
            this.next = next;
        }


        public Node<E> getPrev() {
            return prev;
        }


        public void setPrev(Node<E> prev) {
            this.prev = prev;
        }


        public void setElement(E element) {
            this.element = element;
        }

}


