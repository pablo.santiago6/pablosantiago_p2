package edu.uprm.cse.datastructures.cardealer.util.positionallist;

public interface Position<E> {
	
	public E getElement();

}
